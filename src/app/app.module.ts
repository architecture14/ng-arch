import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ContentProjectionModule } from './features/content-projection/content-projection.module';
import { DemoFormsModule } from './features/demo-forms/demo-forms.module';
import { LocalStorageTestComponent } from './features/local-storage-test/local-storage-test.component';
import { TestModule } from './features/test/test.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent, LocalStorageTestComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    CoreModule,
    SharedModule,
    DemoFormsModule,
    TestModule,
    ContentProjectionModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
