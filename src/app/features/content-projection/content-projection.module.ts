import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ContainerComponent } from './container/container.component';
import { ElementComponent } from './element/element.component';
import { PageComponent } from './page/page.component';

@NgModule({
  declarations: [ContainerComponent, ElementComponent, PageComponent],
  imports: [CommonModule],
})
export class ContentProjectionModule {}
