import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChildren,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ElementComponent } from '../element/element.component';

enum Test {
  Yes = 1,
  No = 2,
}

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent
  implements OnInit, AfterViewInit, AfterContentInit
{
  @ViewChildren(ElementComponent) viewElements!: QueryList<ElementComponent>;
  @ContentChildren(ElementComponent)
  contentElements!: QueryList<ElementComponent>;

  constructor() {
    console.log(Test.Yes);
    let b = null;
    let a = 21;
    b = a;
    a = 42;
    console.log(b);
  }

  ngAfterViewInit(): void {
    console.log('View children', this.viewElements.toArray());
  }

  ngAfterContentInit(): void {
    console.log('Content children', this.contentElements.toArray());
  }

  ngOnInit(): void {}
}
