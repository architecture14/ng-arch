import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from './test.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [TestComponent],
  imports: [CommonModule, SharedModule],
})
export class TestModule {}
