import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { FormResult } from 'src/app/model/form-result';
import { conditionalValidator } from 'src/app/shared/validators/conditional.validator';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss'],
})
export class ReactiveComponent implements OnInit {
  submitted = false;
  demoForm: FormGroup = new FormGroup({});

  demoFormControlKeys: string[] = [];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.demoForm = this.fb.group(
      {
        siren: ['', [Validators.required]],
        nic: [''],
        effectifTotal: ['', Validators.min(10)],
        effectifAssurable: [''],
        dateDebut: [null], // Attention de bien initialiser les dates à null pour ne pas avoir d'erreur de validation
        dateFin: [
          null,
          [
            conditionalValidator(
              () => this.demoForm.get('dateFinObligatoire')?.value, // Ici il faut bien utiliser get pour que cela fonctionne dès le début
              Validators.required
            ),
          ],
        ],
        dateFinObligatoire: [true],
      },
      {
        validators: [this.incoherenceEffectifAssurableVsEffectif],
      }
    );

    this.demoFormControlKeys = Object.keys(this.demoForm.controls);

    // Nécessaire pour faire marcher le validator conditionnel
    this.demoForm.get('dateFinObligatoire')?.valueChanges.subscribe((value) => {
      this.demoForm.get('dateFin')?.updateValueAndValidity();
    });
  }

  incoherenceEffectifAssurableVsEffectif: ValidatorFn = (
    control: AbstractControl
  ): ValidationErrors => {
    const group = control as FormGroup;
    const controlEffectifTotal = group.get('effectifTotal');
    const controlEffectifAssurable = group.get('effectifAssurable');

    console.log(controlEffectifAssurable?.value > controlEffectifTotal?.value);
    const condition =
      Number(controlEffectifAssurable?.value) >
      Number(controlEffectifTotal?.value);

    this.addOrRemoveError(
      controlEffectifTotal,
      { incoherenceEffectifAssurableVsEffectif: true },
      condition
    );
    this.addOrRemoveError(
      controlEffectifAssurable,
      { incoherenceEffectifAssurableVsEffectif: true },
      condition
    );

    return {};
  };

  /**
   * Ajoute ou supprime une erreur dans un champ en fonction de condition
   * Conserve les erreurs existantes sur le champ
   * Si plus aucune erreur n'est présente, met errors à null pour indiquer que le champ est valid
   *
   * @param control champ concerné
   * @param error erreur de validation à ajouter ou supprimer
   * @param condition si true, ajoute l'erreur, sinon la supprime
   */
  addOrRemoveError(
    control: AbstractControl | null,
    error: ValidationErrors,
    condition: boolean
  ): void {
    if (condition) {
      control?.setErrors({
        ...control.errors,
        ...error,
      });
    } else {
      delete control?.errors?.[Object.keys(error)[0]];
      if (Object.keys(control?.errors ?? {}).length === 0 && control) {
        control.setErrors(null);
      }
    }
  }

  addChildForm(name: string, group: FormGroup) {
    this.demoForm.addControl(name, group);
    console.log(this.demoForm);
  }

  submit(): void {
    console.log('submit');
    this.submitted = true;
    const values = this.demoForm.value;
    console.log(values);

    const result = {
      ...values,
      dateDebut: '', // Mapper les dates en str
    } as FormResult;

    console.log(result);
  }

  truc(): void {
    // Modifie des données de façon dynamique sans besoin de spécifier tous les champs
    this.demoForm.patchValue({
      siren: '123456789',
      sousform: {
        nom: 'te',
      },
    });
  }
}
