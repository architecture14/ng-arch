import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sous-formulaire',
  templateUrl: './sous-formulaire.component.html',
  styleUrls: ['./sous-formulaire.component.scss'],
})
export class SousFormulaireComponent implements OnInit {
  @Output() formReady = new EventEmitter<FormGroup>();

  group = this.fb.group({
    nom: ['', [Validators.required]],
    prenom: [''],
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.formReady.emit(this.group);
  }
}
