import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SousFormulaireComponent } from './sous-formulaire.component';

describe('SousFormulaireComponent', () => {
  let component: SousFormulaireComponent;
  let fixture: ComponentFixture<SousFormulaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SousFormulaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SousFormulaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
