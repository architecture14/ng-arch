import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveComponent } from './reactive/reactive.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { TemplateComponent } from './template/template.component';
import { SousFormulaireComponent } from './reactive/sous-formulaire/sous-formulaire.component';

@NgModule({
  declarations: [ReactiveComponent, TemplateComponent, SousFormulaireComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class DemoFormsModule {}
