import { Component, OnInit } from '@angular/core';
import { CacheServiceService } from 'src/app/services/cache-service.service';

@Component({
  selector: 'app-local-storage-test',
  templateUrl: './local-storage-test.component.html',
  styleUrls: ['./local-storage-test.component.scss'],
})
export class LocalStorageTestComponent implements OnInit {
  value?: string;

  constructor(private cacheService: CacheServiceService) {}

  ngOnInit(): void {
    this.get();
  }

  get(): void {
    this.value = this.cacheService.get('test');
  }

  set(): void {
    this.cacheService.set('test', 'truc');
  }
}
