import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CacheServiceService {
  constructor() {}

  get(key: string): any {
    return localStorage.getItem(key);
  }

  set(key: string, value: any): void {
    return localStorage.setItem(key, value);
  }
}
