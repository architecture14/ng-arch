import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from './features/content-projection/page/page.component';
import { ReactiveComponent } from './features/demo-forms/reactive/reactive.component';
import { TemplateComponent } from './features/demo-forms/template/template.component';
import { LocalStorageTestComponent } from './features/local-storage-test/local-storage-test.component';
import { TestComponent } from './features/test/test.component';

const routes: Routes = [
  {
    path: '',
    component: TestComponent,
  },
  {
    path: 'forms/reactive',
    component: ReactiveComponent,
  },
  {
    path: 'forms/template',
    component: TemplateComponent,
  },
  {
    path: 'storage',
    component: LocalStorageTestComponent,
  },
  {
    path: 'content-projection',
    component: PageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
