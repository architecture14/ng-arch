import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DebugReactiveFormComponent } from './components/debug-reactive-form/debug-reactive-form.component';
import { CollapseFlecheComponent } from './components/collapse-fleche/collapse-fleche.component';
import { DebugTemplateFormComponent } from './components/debug-template-form/debug-template-form.component';
import { FormControlErrorsComponent } from './components/form-control-errors/form-control-errors.component';
import { ButtonInterceptorDirective } from './directives/button-interceptor.directive';

@NgModule({
  declarations: [
    DebugReactiveFormComponent,
    CollapseFlecheComponent,
    DebugTemplateFormComponent,
    FormControlErrorsComponent,
    ButtonInterceptorDirective,
  ],
  imports: [CommonModule],
  exports: [
    DebugReactiveFormComponent,
    DebugTemplateFormComponent,
    CollapseFlecheComponent,
    FormControlErrorsComponent,
    ButtonInterceptorDirective,
  ],
})
export class SharedModule {}
