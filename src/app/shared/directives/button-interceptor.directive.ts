import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appButtonInterceptor]',
})
export class ButtonInterceptorDirective implements OnInit {
  constructor(private elementRef: ElementRef) {}

  ngOnInit(): void {
    console.log(this.elementRef);
  }

  @HostListener('click', ['$event'])
  clickEvent(event: any) {
    console.log('Click on button ' + this.elementRef.nativeElement.innerText);
  }
}
