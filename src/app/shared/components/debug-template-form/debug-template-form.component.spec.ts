import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebugTemplateFormComponent } from './debug-template-form.component';

describe('DebugTemplateFormComponent', () => {
  let component: DebugTemplateFormComponent;
  let fixture: ComponentFixture<DebugTemplateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebugTemplateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebugTemplateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
