import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-debug-template-form',
  templateUrl: './debug-template-form.component.html',
  styleUrls: ['./debug-template-form.component.scss'],
})
export class DebugTemplateFormComponent implements OnInit, OnChanges {
  @Input() form!: NgForm;
  formDebug?: FormGroup;

  controlKeys: string[] = [];

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.form.form.controls);
  }

  ngOnInit(): void {
    //this.formDebug = this.form.form;
    //console.log(JSON.stringify(this.form.form));
    //this.controlKeys = Object.keys(this.formDebug.controls);
  }
}
