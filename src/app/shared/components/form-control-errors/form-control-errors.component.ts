import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-control-errors',
  templateUrl: './form-control-errors.component.html',
  styleUrls: ['./form-control-errors.component.scss'],
})
export class FormControlErrorsComponent implements OnInit {
  @Input() errors: any;
  @Input() name!: string;

  /**
   * Composant pour afficher les erreurs 'standard' des input
   */
  constructor() {}

  ngOnInit(): void {}
}
