import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-debug-reactive-form',
  templateUrl: './debug-reactive-form.component.html',
  styleUrls: ['./debug-reactive-form.component.scss'],
})
export class DebugReactiveFormComponent implements OnInit {
  @Input() form?: FormGroup | null;
  @Input() name?: string;

  controlKeys: string[] = [];

  constructor() {}

  ngOnInit(): void {
    if (this.form) {
      this.controlKeys = Object.keys(this.form.controls);
    }
  }
}
