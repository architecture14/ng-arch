import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebugReactiveFormComponent } from './debug-reactive-form.component';

describe('DebugReactiveFormComponent', () => {
  let component: DebugReactiveFormComponent;
  let fixture: ComponentFixture<DebugReactiveFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebugReactiveFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebugReactiveFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
