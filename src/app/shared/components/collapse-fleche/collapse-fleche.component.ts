import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-collapse-fleche',
  templateUrl: './collapse-fleche.component.html',
  styleUrls: ['./collapse-fleche.component.scss'],
})
export class CollapseFlecheComponent implements OnInit {
  // Etat initial
  @Input() startOpen = false;

  // Etat courant
  isCollapsed = true;

  constructor() {}

  ngOnInit(): void {
    if (this.startOpen) {
      this.isCollapsed = false;
    }
  }
}
