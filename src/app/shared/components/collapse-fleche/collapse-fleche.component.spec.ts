import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapseFlecheComponent } from './collapse-fleche.component';

describe('CollapseFlecheComponent', () => {
  let component: CollapseFlecheComponent;
  let fixture: ComponentFixture<CollapseFlecheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollapseFlecheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapseFlecheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
