export interface FormResult {
  siren: string;
  nic: string;
  dateDebut: string;
  dateFin: string;
}
